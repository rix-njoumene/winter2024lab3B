public class Lynx{
	
	private String furColor;
	public String furType;
	public int sigthRange;
	
	//getter
	public String getFurColor(){
		return this.furColor;
	}
	public String getFurType(){
		return this.furType;
	}
	public int getSigthRange(){
		return this.sigthRange;
	}
	
	//setter
	public void setFurColor(String furColor){
		
		if(furColor.equals("white")){
			this.furColor = furColor;
		}
		else if(furColor.equals("brown")){
			this.furColor = furColor;
		}
		else{
			this.furColor = "beige";
		}
	}
	
	//first action
	public void sneak(){
		if(furColor.equals("white")){
			System.out.println("You sneaked!");
		}
	}
	
	public void farSigth(){
		if(sigthRange > 10){
			System.out.println("You looked 20 meters away! \nBut there was nothing");
		}
		else if(sigthRange > 20){
			System.out.println("You looked 40 meters away! \nYou saw a rabbit; Let the HUNT start!");
		}
		else{
			System.out.println("You looked 10 meters away! \nBut there was nothing");
		}
	}
}